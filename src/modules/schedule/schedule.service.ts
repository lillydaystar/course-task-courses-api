import {BadRequestException, Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import {IScheduleItem} from "./types";
import axios from "axios";
import * as cheerio from 'cheerio';
import {CourseSeason, EducationLevel} from "../../common/types";
import * as https from "https";

@Injectable()
export class ScheduleService {

    private dateReformat(date: string): string {
        let str =  date.replace('(оновлено: ', '').replace(')', '').trim();
        const dayAndTime = str.split(' ');
        let day = dayAndTime[0].split('.');
        let time = dayAndTime[1].split(':');

        // input format: 'DD.MM.YYYY HH:mm:ss'
        // output format: 'YYYY-MM-DD HH:mm:ss'
        str = day[2] + '-' + day[1] + '-' + day[0]
            + ' ' + time[0] + ':' + time[1] + ':' + time[2];
        return str;
    }
    async getScheduleInfo(year: number, season: string): Promise<IScheduleItem[]> {
        let trym: number;
        let seasonRes: CourseSeason;
        let urlYear = year;
        switch (season) {
            case 'autumn':
                trym = 1;
                seasonRes = CourseSeason.AUTUMN;
                break;
            case 'spring':
                urlYear--;
                trym = 2;
                seasonRes = CourseSeason.SPRING;
                break;
            case 'summer':
                urlYear--;
                trym = 3;
                seasonRes = CourseSeason.SUMMER;
                break;
            default:
                throw new BadRequestException('Wrong url (unknown season value) :(');
        }
        const url = `https://my.ukma.edu.ua/schedule/?year=${urlYear}&season=${trym}`;

        const response = await axios.get(url, {httpsAgent: new https.Agent({rejectUnauthorized: false})})
            .then(response => {return response;})
            .catch(() => {
                throw new InternalServerErrorException('Cannot connect to САЗ :(')
            });

        const $ = cheerio.load(response.data);
        const faculties = $('#schedule-accordion').children();

        const scheduleArray: IScheduleItem[] = [];
        faculties.each((index, faculty) => {
            const years = $(faculty).find('.panel-collapse .panel-body .panel-group').children();
            const facultyName = $(faculty).find('.panel-heading h4.panel-title a').first().text().trim();
            years.each((indexY, course) => {
                const specialities = $(course).find('.list-group').children();
                const courseInfo = $(course).find('.panel-heading .panel-title a').text().trim();
                const match = courseInfo.match(/([БМ]П), (\d) рік навчання/);
                let level: EducationLevel;
                let year: 1 | 2 | 3 | 4;
                if (match) {
                    if (match[1] === 'БП') {
                        level = EducationLevel.BACHELOR;
                    } else level = EducationLevel.MASTER;
                    year = parseInt(match[2]) as 1 | 2 | 3 | 4;
                }
                specialities.each((indexS, speciality) => {
                    let dateString = $(speciality).find('span.pull-right.small.text-muted').text();
                    dateString = this.dateReformat(dateString);

                    const urlDocx = $(speciality).find('a[title^=Переглянути]').attr('href') as string;
                    const title = $(speciality).last().text().trim();
                    const match = title.match(/((.*) ([БМ]П)-(\d)) (.*)/);
                    let specialityName: string = '';
                    if (match) {
                        specialityName = match[2].trim();
                    }
                    scheduleArray.push({
                        url: urlDocx,
                        updatedAt: dateString,
                        facultyName: facultyName,
                        specialityName: specialityName,
                        level: level,
                        year: year,
                        season: seasonRes,
                    })
                })
            });
        });
        if (scheduleArray.length === 0) {
            throw new NotFoundException('There are no schedules available :(');
        }
        return scheduleArray;

    }
}
