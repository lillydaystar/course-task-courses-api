import {Controller, Get, Param, ParseIntPipe} from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import {IScheduleItem} from "./types";

@Controller('schedule')
export class ScheduleController {
  constructor(
    protected readonly service: ScheduleService,
  ) {}

  @Get(':year/:season')
  public async getSchedule(@Param('year', new ParseIntPipe()) year: number,
                           @Param('season') season: string): Promise<IScheduleItem[]> {
    return this.service.getScheduleInfo(year, season);
  }
}
