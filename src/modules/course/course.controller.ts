import {Body, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Post, Query} from '@nestjs/common';
import { CourseService } from './course.service';
import {CourseFeedbackDto, ICourse} from "./types";
import {CourseFeedback} from "../../models/entities/db/CourseFeedback.entity";
import {Pagination, IPaginationOptions} from 'nestjs-typeorm-paginate';

@Controller('course')
export class CourseController {
  constructor(
    protected readonly service: CourseService,
  ) {}

  @Post(':code/review')
  async createCourseReview(options: IPaginationOptions,
    @Param('code', new ParseIntPipe()) code: number,
    @Body() review: CourseFeedbackDto): Promise<CourseFeedback> {
    return this.service.sendReview(code, review);
  }

  @Get(':code/reviews')

  async getCourseReviews(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Param('code', new ParseIntPipe()) code: number):
    Promise<{ rating: number, ratingCount: number, items: Pagination<CourseFeedback> }> {
    //return this.service.getReviews(code);
    limit = limit > 100 ? 100 : limit;
    return this.service.getReviews({page, limit}, code);
  }

  @Get(':code')
  public async getCourse(@Param('code', new ParseIntPipe()) // ParseIntPipe() throws error 400 if code type is not number
                               code: number): Promise<{course: ICourse, rating?: number}> {
    return this.service.getCourseInfo(code);
  }
}
