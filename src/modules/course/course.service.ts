import {Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import axios from 'axios';
import * as cheerio from 'cheerio';
import {CourseFeedbackDto, ICourse} from './types';
import {CourseSeason, EducationLevel} from "../../common/types";
import * as https from "https";
import {CourseFeedback} from "../../models/entities/db/CourseFeedback.entity";
import {Course} from "../../models/entities/db/Course.entity";
import {paginate, Pagination, IPaginationOptions} from 'nestjs-typeorm-paginate';

@Injectable()
export class CourseService {

  async sendReview(code: number, review: CourseFeedbackDto): Promise<CourseFeedback> {
    let course = await Course.findOne({ code: code });
    if (!course) {
      await this.addCourse(code);
      course = await Course.findOne({ code: code });
      if (!course) throw new NotFoundException(`Course with code ${code} not found`);
    }
    const feedback = new CourseFeedback();
    feedback.course = course;
    feedback.rating = review.rating;
    feedback.text = review.text;
    return await feedback.save();
  }
  async getReviews(options: IPaginationOptions, code: number):
    Promise<{ rating: number, ratingCount: number, items: Pagination<CourseFeedback> }> {
    let course = await Course.findOne({ code: code });
    if (!course) {
      await this.addCourse(code);
      course = await Course.findOne({ code: code });
      if (!course) throw new NotFoundException(`Course with code ${code} not found`);
    }
    const reviews = await CourseFeedback.find({where: { course: { code: course.code }}});
    if (!reviews || reviews.length === 0) {
      throw new NotFoundException(`Feedbacks for this course (${code}) not found`);
    }
    const rating = reviews.reduce((acc, review) => acc + review.rating, 0) / reviews.length;
    const ratingCount = reviews.length;
    const query = await CourseFeedback.createQueryBuilder("course_feedback")
      .where("course_feedback.course_code = :code", { code: course.code })
    const items = await paginate<CourseFeedback>(query, options);
    return {
      rating,
      ratingCount,
      items,
    };
  }

  async createCourse(course: ICourse): Promise<Course> {
    const newCourse = new Course();
    newCourse.code = course.code;
    newCourse.name = course.name;
    newCourse.description = course.description;
    newCourse.facultyName = course.facultyName;
    newCourse.departmentName = course.departmentName;
    newCourse.level = course.level;
    newCourse.year = course.year;
    newCourse.seasons = course.seasons;
    newCourse.creditsAmount = course.creditsAmount;
    newCourse.hoursAmount = course.hoursAmount;
    newCourse.teacherName = course.teacherName;

    return newCourse.save();
  }

  async addCourse(code: number): Promise<Course> {
    const courseInfo = await this.getCourseInfo(code);
    return await this.createCourse(courseInfo.course);
  }
    async getCourseInfo(code: number): Promise<{course: ICourse, rating?: number}> {
        const url = `https://my.ukma.edu.ua/course/${code}`;
        const response = await axios.get(url, {httpsAgent: new https.Agent({rejectUnauthorized: false})})
            .then(response => {return response;})
            .catch((err) => {
                if(err.response && err.response.status === 404) { // throws error 404 is there is no such course
                    throw new NotFoundException('Cannot find specified course :(')
                }
                else // Throws error 500 is there is no connection with САЗ
                    throw new InternalServerErrorException('Cannot connect to САЗ :(')
            });
        const $ = cheerio.load(response.data);
        const name = $('.page-header h1').contents().filter(function() {
            return this.nodeType === 3; // Вибираємо тільки текстові вузли
        }).text().trim();
        const credits = parseInt($("table.table tbody:eq(0) .label[title*='кредитів']").text());
        const description = $(`#course-card--${code}--info`).text().trim();
        const facultyName = $('table.table tr:eq(2) td').text();
        const departmentName = $('table.table tr:eq(3) td').text();
        const year  = parseInt($("table.table tbody:eq(0) .label[title*='рік викладання']").text());
        const hours  = parseInt($("table.table tbody:eq(0) .label[title*='годин']").text());
        const teacher = $('table.table tr:eq(6) th').next().text();
        const levelStr = $('table.table tr:eq(4) td').text();

        let yearNumber = year as 1 | 2 | 3 | 4;

        let level: EducationLevel;
        if (levelStr === 'Бакалавр') {
            level = EducationLevel.BACHELOR;
        } else {
            level = EducationLevel.MASTER;
        }

        const seasonObj = $('table.table tbody:eq(2) tr:eq(1) th').next().children();
        const seasons = seasonObj.map((index, str) => {
            const match = $(str).text().match(/Семестр (\d+)(д?)/);
            if (match) {
                const number = parseInt(match[1]);
                if (number % 2 === 0) {
                    if (match && (match[2] === 'д')) {
                        return CourseSeason.SUMMER;
                    }
                    return CourseSeason.SPRING;
                }
                else {
                    return CourseSeason.AUTUMN;
                }
            }
        }).get();

        let rating: number | undefined;
        const course = await Course.findOne({ code: code });
        if (!course) {
          rating = undefined;
        }
        else {
          const reviews = await CourseFeedback.find({where: { course: { code: course.code }}});
          if (!reviews || reviews.length === 0) {
            rating = undefined;
          }
          else {
            rating = reviews.reduce((acc, review) => acc + review.rating, 0) / reviews.length;
          }
        }

        return {
          course : {
            code: code,
            name: name,
            description: description !== '' ? description: undefined,
            facultyName: facultyName,
            departmentName: departmentName,
            level: level,
            year: yearNumber,
            seasons: seasons,
            creditsAmount: credits !== 0 ? credits: undefined,
            hoursAmount: hours !== 0 ? hours: undefined,
            teacherName: teacher !== '' ? teacher: undefined,
          },
          rating: rating
        };
    }
}
