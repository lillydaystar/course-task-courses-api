import { CourseSeason, EducationLevel } from '../../common/types';
import {IsInt, Max, Min} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export interface ICourse {
  code: number;
  name: string;
  description?: string;
  facultyName: string; // Назва факультету
  departmentName: string; // Назва кафедри
  level: EducationLevel;
  year: 1 | 2 | 3 | 4;
  seasons: CourseSeason[];
  creditsAmount?: number;
  hoursAmount?: number;
  teacherName?: string;
}

export class CourseFeedbackDto {
  @IsInt()
  @Min(1)
  @Max(10)
  @ApiProperty( {description: 'Рейтинг курсу', minimum: 1, maximum: 10})
  rating: number;

  @ApiProperty({description: 'Текст відгуку'})
  text: string;
}
