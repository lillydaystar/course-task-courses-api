import {Entity, Column, PrimaryGeneratedColumn, OneToMany, BaseEntity} from 'typeorm';
import { CourseFeedback } from './CourseFeedback.entity';
import {CourseSeason, EducationLevel} from "../../../common/types";

@Entity({ name: 'course' })
export class Course extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'code', type: "int" })
  code: number;

  @Column({ name: 'name', type: "varchar" })
  name: string;

  @Column({ name: 'description', nullable: true, type: "varchar" })
  description?: string;

  @Column({ name: 'faculty_name', type: "varchar" })
  facultyName: string;

  @Column({ name: 'department_name', type: "varchar" })
  departmentName: string;

  @Column({ name: 'level', type: "enum", enum: EducationLevel })
  level: EducationLevel;

  @Column({ name: 'year', type: "int" })
  year: 1 | 2 | 3 | 4;

  @Column({ name: 'seasons', type: 'simple-array', enum: CourseSeason })
  seasons: CourseSeason[];

  @Column({ name: 'credits_amount', nullable: true, type: "int" })
  creditsAmount?: number;

  @Column({ name: 'hours_amount', nullable: true, type: "int" })
  hoursAmount?: number;

  @Column({ name: 'teacher_name', nullable: true, type: "varchar" })
  teacherName?: string;

  @OneToMany(() => CourseFeedback, feedback => feedback.course)
  feedbacks?: CourseFeedback[];
}
