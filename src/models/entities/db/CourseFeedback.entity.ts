import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, BaseEntity} from 'typeorm';
import {Course} from "./Course.entity";

@Entity({ name: 'course_feedback' })
export class CourseFeedback extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'course_id', type: 'int' })
  courseId: number;

  @Column({ name: 'rating', type: 'tinyint' })
  rating: number;

  @Column({ name: 'text', type: 'text', nullable: true })
  text?: string;

  @Column({name: "course_code"})
  courseCode: number

  @ManyToOne(() => Course, course => course.feedbacks)
  @JoinColumn({name: 'course_code'})
  course: Course;
}
