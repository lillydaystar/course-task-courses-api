import {MigrationInterface, QueryRunner} from "typeorm";
import {CourseFeedback} from "../models/entities/db/CourseFeedback.entity";
import {Course} from "../models/entities/db/Course.entity";
import {CourseSeason, EducationLevel} from "../common/types";

export class InsertData1682265400049 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.manager.insert(Course, {
        code: 275844,
        name: 'Управління цифровим продуктом',
        description: 'Цей курс дає базове розуміння якими компетенціями повинен володіти спеціаліст, що займається розвитком і управлінням цифрового продукту. Все починається з ідеї, визначення цінності, дослідження користувачів і перевірки початкових гіпотез. На основі зібраної інформації формується ціннісна пропозиція, розробляються інтерактивні прототипи, описуються вимоги за допомогою карт користувацьких історій та формалізуються процеси за допомогою діаграми BPMN.\nПрактична робота проходить в командах, які працюють над ідеями цифрових продуктів обираними студентамии самостійно.\nЦей курс є частиною сертифікатної програми «Дизайн цифрових продуктів».',
        facultyName: 'Факультет інформатики',
        departmentName: 'Кафедра мультимедійних систем',
        level: EducationLevel.BACHELOR,
        year: 2,
        seasons: [CourseSeason.SPRING],
        creditsAmount: 3,
        hoursAmount: 90,
        teacherName: 'Корнійчук М.А.',
      });
      await queryRunner.manager.insert(Course, {
        code: 275947,
        name: 'Інструменти та принципи веб-розробки',
        facultyName: 'Факультет інформатики',
        departmentName: 'Кафедра інформатики',
        level: EducationLevel.BACHELOR,
        year: 3,
        seasons: [CourseSeason.AUTUMN],
        creditsAmount: 3,
        hoursAmount: 90,
      });
      await queryRunner.manager.insert(Course, {
        code: 275949,
        name: 'Backend-розробка на базі NodeJS',
        facultyName: 'Факультет інформатики',
        departmentName: 'Кафедра інформатики',
        level: EducationLevel.BACHELOR,
        year: 3,
        seasons: [CourseSeason.SPRING],
        creditsAmount: 4,
        hoursAmount: 120,
        teacherName: 'Кобзар Олег Олегович',
      });
      await queryRunner.manager.insert(Course, {
        code: 284166,
        name: 'Бази даних',
        facultyName: 'Факультет інформатики',
        departmentName: 'Кафедра інформатики',
        level: EducationLevel.BACHELOR,
        year: 2,
        seasons: [CourseSeason.AUTUMN, CourseSeason.SPRING],
        creditsAmount: 8,
        hoursAmount: 240,
      });
      await queryRunner.manager.insert(Course, {
        code: 284167,
        name: 'Теорія еволюції (1р.н.)',
        facultyName: 'Факультет природничих наук',
        departmentName: 'Кафедра біології',
        level: EducationLevel.BACHELOR,
        year: 4,
        seasons: [CourseSeason.AUTUMN],
        creditsAmount: 3,
        hoursAmount: 90,
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 10,
        text: 'Найкращий курс у могилянці!',
        courseCode: 275949
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 2,
        text: 'Я двієчник',
        courseCode: 275949
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 9,
        courseCode: 275949
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 8,
        text: 'Норм',
        courseCode: 275949
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 9,
        text: 'Цікаво класні лекції',
        courseCode: 275949
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 5,
        text: 'Шо це',
        courseCode: 275947
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 7,
        courseCode: 275947
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 8,
        text: 'класні лаби',
        courseCode: 275947
      });
      await queryRunner.manager.insert(CourseFeedback, {
        rating: 10,
        text: 'я всім ставлю 10 щоб мати зарах',
        courseCode: 275947
      });


    }


  public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.manager.delete(CourseFeedback, {});
      await queryRunner.manager.delete(Course, {});
    }

}
