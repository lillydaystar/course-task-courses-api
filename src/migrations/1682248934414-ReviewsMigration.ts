import {MigrationInterface, QueryRunner} from "typeorm";

export class ReviewsMigration1682248934414 implements MigrationInterface {
    name = 'ReviewsMigration1682248934414'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`course_feedback\` (\`course_id\` int NOT NULL AUTO_INCREMENT, \`rating\` tinyint NOT NULL, \`text\` text NULL, \`course_code\` int NULL, PRIMARY KEY (\`course_id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`course\` (\`code\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`description\` varchar(3000) NULL, \`faculty_name\` varchar(255) NOT NULL, \`department_name\` varchar(255) NOT NULL, \`level\` enum ('Bachelor', 'Master') NOT NULL, \`year\` int NOT NULL, \`seasons\` text NOT NULL, \`credits_amount\` int NULL, \`hours_amount\` int NULL, \`teacher_name\` varchar(255) NULL, PRIMARY KEY (\`code\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`course_feedback\` ADD CONSTRAINT \`FK_8938364e2492958e91bc4a0bb36\` FOREIGN KEY (\`course_code\`) REFERENCES \`course\`(\`code\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`course_feedback\` DROP FOREIGN KEY \`FK_8938364e2492958e91bc4a0bb36\``);
        await queryRunner.query(`DROP TABLE \`course\``);
        await queryRunner.query(`DROP TABLE \`course_feedback\``);
    }

}
